#!/bin/bash


: '
    Copyright (C) 2017 Pierre Clavel (pititux@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'




dir=$(dirname "$0")  # 'dirname $0' renvoie le nom du répertoire dans lequel le script actuel se trouve.
. "${dir}"/files/config.sh

cd "${dir}/${SCRIPT_FILES_PATH}"

# Rendre les scripts exécutables
#chmod 777 "${SCRIPT_FILE_NAME_0}" "${SCRIPT_FILE_NAME_1}" "${SCRIPT_FILE_NAME_2}" "${SCRIPT_FILE_NAME_3}"
if [ $? -ne 0 ]
then
    echo "    ERREUR lors de l'accès aux scripts."
    echo "    Certains fichiers doivent être manquants ou inaccessibles."
    exit 21
fi


echo; echo "Démarrage..."
if [ $EUID -eq 0 ]
then
    bash "./${SCRIPT_FILE_NAME_0}"
else
    echo "Veuillez saisir (discrètement !) le mot de passe root (administrateur) du système :"
    su -c "bash ./${SCRIPT_FILE_NAME_0}"
fi
if [ $? -ne 0 ]; then
    echo "    ERREUR lors du lancement du script principal."
    exit 21
fi


exit 0


