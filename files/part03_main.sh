#!/bin/bash


: '
    Copyright (C) 2017 Pierre Clavel (pititux@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'



# SCRIPT 3 : Affichage d'informations. 


# import functions and config files
dir=$(dirname "$0")  # 'dirname $0' renvoie le nom du répertoire dans lequel le script actuel se trouve.
. "${dir}"/config.sh
. "${dir}"/functions/display
. "${dir}"/functions/utils


#-----------------------------------
# Debut du programme de la partie 3
#-----------------------------------

clear
print_menu_part3
pause

exit 0

