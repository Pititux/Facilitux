#!/bin/bash


: '
    Copyright (C) 2017 Pierre Clavel (pititux@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'



# import functions
dir=$(dirname "$0")  # 'dirname $0' renvoie le nom du répertoire dans lequel le script actuel se trouve.
. "${dir}"/config.sh
. "${dir}"/functions/display
. "${dir}"/functions/utils

while [ true ]
do
    clear
    print_main_menu
    read -p "  Choix : " choice
    case $choice in
        1) bash "./$SCRIPT_FILE_NAME_1" ;;
        2) bash "./$SCRIPT_FILE_NAME_2" ; exit 0 ;;
        3) bash "./$SCRIPT_FILE_NAME_3" ;;
        0) echo; exit 0 ;;
        #*) echo -e "Erreur de saisie." ;;
    esac
done 

exit 0


