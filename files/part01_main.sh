#!/bin/bash


: '
    Copyright (C) 2017 Pierre Clavel (pititux@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'



# SCRIPT 1 : Configuration du système.


# import functions and config files
dir=$(dirname "$0")  # 'dirname $0' renvoie le nom du répertoire dans lequel le script actuel se trouve.
. "${dir}"/config.sh
. "${dir}"/functions/display
. "${dir}"/functions/utils

declare -a line_array  # array to store lines from files read


#-- LOCAL FUNCTIONS ---------------------------------------------------

read_config_file()
{
    local filename="$*"
    line_array=()  # to empty array
    
    while IFS='' read -r line || [[ -n "$line" ]]; do
        # IFS='' prevents leading/trailing whitespace from being trimmed.
        # -r prevents backslash escapes from being interpreted.
        # || [[ -n $line ]] prevents the last line from being ignored if it doesn't end with a \n 

        line=$(echo "$line" | sed 's/#.*$//')      # delete comments
        
        line=$(echo "$line" | sed 's/[[:blank:]]*$//' ) # remove all possible spaces at the end of line
        line=$(echo "$line" | sed 's/[[:space:]]\+/ /g' ) # delete spaces and tabs at the begin of line
        #    [   # start of character class
        #    [:space:]  # The POSIX character class for whitespace characters. It's
                        # functionally identical to [ \t\r\n\v\f] which matches a space,
                        # tab, carriage return, newline, vertical tab, or form feed.
        #    [:blank:]  # Is only horizontal whitespace (space and tab). 
        #    ]   # end of character class
        #    \+  # one or more of the previous item (anything matched in the brackets).
        
        # if line length greater or equal 2 characters : add this line to the array
        if [ ${#line} -ge 2 ]; then line_array=("${line_array[@]}" "$line"); fi
        
    done < $filename
}


add_apt_key()
{
	# apt-key list
	# u=$( echo "$k" | sed 's/[[:blank:]]//g' ) # delete all spaces
	# echo ${u: -16}    # keep only 16 last chars
    local key="$1"
    print_message_arrow " Install key : ${key}"
    if [ ${#key} -ge 8 ]; then
        if [[ ${key} == *".asc"* ]]; then               # .asc file 
            exec_multi wget -q ${key} -O- -o /dev/null | apt-key add -
        elif [[ ${key} =~ ^[A-Fa-f0-9]*$ ]]; then       # hex key
            exec_multi apt-key adv --keyserver ha.pool.sks-keyservers.net --recv-keys ${key}
        else
            echo "  Erreur ! Clé invalide dans le fichier : "${key}
            exit 3
        fi
    fi
    exit_if_error "lors de l'ajout de la clé vers Apt." silent_ok
}

install_with_apt()
{
    local pkg_name="$*"
    # INSTALL A PACKAGE WIDTH APT
    dpkg --verify "${pkg_name}" &> /dev/null
    if [ $? -ne 0 ]; then
        print_message_arrow "Installation du paquet ${pkg_name}..."
        apt --yes install "${pkg_name}"
        exit_if_error " lors de l'installation de ${pkg_name}." up
    fi
}

#-- FUNCTIONS FOR ACTIONS -----------------------------------------------


apt_source_update()
{
    ### ACTION 1 - Install apt keys and new sources.list file ###
    echo
    echo "  Attention :"
    echo "    Vous pouvez maintenant modifier les fichiers"
    echo "    sources.list et softwares_keys, issus de"
    echo "    Facilitux, avant de poursuivre."
    echo "  Attention :"
    echo "    Le fichier sources.list du système va être écrasé."
    echo "    Une copie du fichier actuel va être réalisée."
    echo "  Voulez-vous continuer ?"
    ask_yes_no
    if [ $? -ne 0 ]; then
        echo "  D'accord. Rien n'a été modifié."
        return 0
    fi
    
    # BACKUP SOURCES.LIST FILE
    print_message_arrow "Sauvegarde de ${SOURCESLIST_FILENAME}..."
    filename="${SOURCESLIST_FILENAME}_old-"`date "+%Y%m%d-%H%M%S"`
    cp "/etc/apt/${SOURCESLIST_FILENAME}" "/etc/apt/${filename}"
    exit_if_error " problème lors de la copie." up

    # CREATING TEMP SOURCE.LIST FILE
    print_message_arrow "Création d'un source.list temporaire..."
    cat "./../${SYSTEM_FILES_PATH}${SOURCESLIST_TEMP_FILENAME}" > "/etc/apt/${SOURCESLIST_FILENAME}"
    exit_if_error " problème de modification de sources.list." up
    
    # UPDATE APT
    apt update
    exit_if_error " lors du Apt update." silent_ok
    export DEBIAN_FRONTEND=noninteractive  # to avoid stdin error message
    
    # INSTALL PACKAGES REQUIRED
    install_with_apt wget
    install_with_apt dirmngr
    install_with_apt debian-archive-keyring
    install_with_apt apt-transport-https   # (required for https repositories)

    # INSTALL DEB-MULTIMEDIA-KEYRING PACKAGE
    dpkg --verify deb-multimedia-keyring &> /dev/null
    if [ $? -ne 0 ]; then
        print_message_arrow "Installation du paquet deb-multimedia-keyring..."
        #wget http://www.deb-multimedia.org/pool/main/d/deb-multimedia-keyring/deb-multimedia-keyring_2016.8.1_all.deb && dpkg -i deb-multimedia-keyring_2016.8.1_all.deb
        link_path="https://www.deb-multimedia.org"$( wget -q http://www.deb-multimedia.org/dists/stable/main/binary-amd64/package/deb-multimedia-keyring -O - | grep deb-multimedia-keyring_ | sed -n "/href/ s/.*href=['\"]\([^'\"]*\)['\"].*/\1/gp" )
        exit_if_error " lors de la récupération de l'url de deb-multimedia-keyring."
        install_deb $link_path
        exit_if_error " lors de l'installation de deb-multimedia-keyring." silent_ok
    fi
    
    # ADD APT KEYS
    print_message_arrow "Lecture du fichier ${SOFTWARES_KEYS_FILENAME}..."
    read_config_file "./../${SOFTWARES_KEYS_FILENAME}"
    exit_if_error " lors de la lecture de ${SOFTWARES_KEYS_FILENAME}." up
    print_message_arrow "Ajout des clés pour Apt..."
    for i in "${line_array[@]}"; do
        add_apt_key "${i}"
    done
    echo "   Fin de l'importation des clés."
    exit_if_error " lors de l'ajout des clés pour Apt." up
    
    # COPY DEFINITIVE SOURCES.LIST FILE
    print_message_arrow "Copie du ${SOURCESLIST_FILENAME} définitif..."
    cp -f "./../${SOURCESLIST_FILENAME}" "/etc/apt/${SOURCESLIST_FILENAME}" \
      && chmod 644 "/etc/apt/${SOURCESLIST_FILENAME}"
    exit_if_error " problème lors de la copie." up
}


apt_upgrade()
{
    ### ACTION 2 - Upgrade system with Apt ###
    print_message_arrow "Mises à jour du système..."
    apt update
    exit_if_error "lors du téléchargement de la liste des paquets d'apt."
    apt upgrade
    exit_if_error "lors de la mise à jour du système."
    echo "     Mises à jour du système terminées."
}


apt_softwares_action()
{
    local action="${1}"
    local filename="${2}"

    ### ACTION 3 and 4 - Install or remove softwares ###
    echo "  Attention, vous pouvez encore modifier le fichier"
    echo "  \"${filename}\" avant de continuer."
    pause
    
    print_message_arrow "Lecture du fichier..."
    read_config_file "${filename}"
    exit_if_error "Problème de lecture du fichier." up

    local list_string=""
    local list_array=()
    for i in "${line_array[@]}"; do list_string="${list_string} ${i}"; done # array to string
    list_array=($list_string)   # string to array

    print_message_arrow "Téléchargement de la liste des paquets..."
    apt update &> /dev/null
    exit_if_error "Problème lors du apt update (clés manquantes ?)." up

    print_message_arrow "Vérification des noms de paquets..."
    apt --simulate --yes install $list_string &> /dev/null
    if [ $? -ne 0 ]; then
    echo "      Erreur dans le fichier ${filename}"
    echo "      Recherche des noms de paquets erronés..."
    for i in ${list_array[@]}; do
        if [[ -z $( apt-cache search ^"${i}"$ ) ]]; then echo "        ${i}"; fi
    done
    echo "      Modifiez le fichier avant de recommencer."
    pause; exit 3
    fi
    exit_if_error "Problème dans les noms de paquets." up

    print_message_arrow "Installation des paquets..."
    apt "${action}" ${list_string}
    exit_if_error "Problème rencontré lors du Apt ${action}." silent_ok

    return 0
}


apt_cleanup()
{
    ### ACTION 5 - Clean system with Apt ###
    echo "Nettoyage d'apt..."
    apt -V autoremove
    apt clean
    echo "Espace libre :"
    df -h
    return 0
}


conf_grub_timeout()
{
    ### ACTION 6 - Reduce grub timeout when computer start ###
    echo "  Réduction du temps d'attente du bootloader..."
    print_message_arrow "Modification du fichier de conf..."
    sed -i 's/.*GRUB_TIMEOUT=.*/GRUB_TIMEOUT=1/' /etc/default/grub
    exit_if_error "modification de /etc/default/grub impossible." up
    print_message_arrow "Mise à jour de Grub..."
    update-grub
    exit_if_error "lors de la mise à jour de Grub."
}

#-----------------------------------------------------



#------------------------------------
# Debut du programme de la partie 1
#------------------------------------

clear
print_title "PARTIE 1 - CONFIGURATION DU SYSTÈME"


print_message_arrow "Vérification des permissions..."
[ $EUID -eq 0 ]
exit_if_error "Le script doit être lancé par root." up


print_message_arrow "Présence des fichiers requis..."
[ -e "./../sources.list" ] && [ -e "./../${SOFTWARES_INSTALL_FILENAME}" ]
exit_if_error "Il manque des fichiers requis par le script." up


print_message_arrow "Test de l'accès à Internet..."
exec_multi ping -c 1 ftp.fr.debian.org &> /dev/null
exit_if_error "Problème d'accès à Internet." up


if [ ${PROXY} ]; then
	print_message_arrow "Export du proxy..."
	export http_proxy=$PROXY
	export https_proxy=$PROXY
	export ftp_proxy=$PROXY
	exit_if_error "Problème lors de l'export du proxy." up
fi


while true; do
    pause
    clear
    print_title "PARTIE 1 - CONFIGURATION DE DEBIAN"
    print_menu_part1
    read -p "    Choix : " choix
    case $choix in
        1 ) apt_source_update ;;
        2 ) apt_upgrade ;;
        3 ) apt_softwares_action install "./../$SOFTWARES_INSTALL_FILENAME" ;;
        4 ) apt_softwares_action remove "./../$SOFTWARES_REMOVE_FILENAME" ;;
        5 ) apt_cleanup ;;
        6 ) conf_grub_timeout ;;
        0 ) break ;;
        * ) echo "  Veuillez saisir une valeur du menu." ;;
    esac
done

exit 0


