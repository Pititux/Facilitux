#!/bin/bash


: '
    Copyright (C) 2017 Pierre Clavel (pititux@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'



# SCRIPT 2 : Configuration de Xfce.


# import functions and config files
dir=$(dirname "$0")  # 'dirname $0' renvoie le nom du répertoire dans lequel le script actuel se trouve.
. "${dir}"/config.sh
. "${dir}"/functions/display
. "${dir}"/functions/utils
. "${dir}"/../xfce_configuration




#-----------------------------------------------------

conf_xfce_theme()
{
    echo "  Modification du thème..."
    
    print_message_arrow "Changement du fond d'écran actuel..."
    export -f command_line_set-background
    su $USER_NAME -c 'command_line_set-background'
    exit_if_error " Oups :(" up
        
    print_message_arrow "Changement des icones et du style..."
    export -f command_line_set-theme
    su $USER_NAME -c 'command_line_set-theme'
    exit_if_error " Oups :(" up
    
    print_message_arrow "Configuration des ombres des fenêtres..."
    # Use xfce4-settings-editor to find elements.
    export -f command_line_set-compositing
    su $USER_NAME -c 'command_line_set-compositing'
    exit_if_error "lors de la configuration de xfwm4." up
    su $USER_NAME -c 'xfwm4 --replace &' &> /dev/null && sleep 1
    exit_if_error "lors de la configuration de xfwm4." silent_ok
}


conf_xfce_autologin()
{
    print_message_arrow "Configuration de la connexion automatique..."
    sed -i "s/^#autologin-user=.*/autologin-user=$USER_NAME/" /etc/lightdm/lightdm.conf
    sed -i "s/^autologin-user=.*/autologin-user=$USER_NAME/" /etc/lightdm/lightdm.conf
    exit_if_error "lors de la configuration de lightdm." up
}


conf_xfce_backgrounds()
{
    echo "  Installation des fonds d'écrans..."
    if [ ! -d ${BACKGROUNDS_TARGET_DIRECTORY} ]; then
        print_message_arrow "Création du répertoire cible..."
        mkdir -p "${BACKGROUNDS_TARGET_DIRECTORY}"
        exit_if_error "lors de la création de ${BACKGROUNDS_TARGET_DIRECTORY}" up
    fi
    print_message_arrow "Copie des fichiers..."
    cp -R ./${BACKGROUNDS_SOURCE_DIRECTORY}* ${BACKGROUNDS_TARGET_DIRECTORY}
    exit_if_error "lors de la copie des fichiers." up
}


conf_xfce_panel()
{
    print_message_arrow "Modification du panneau..."
    export -f xfconf_panel && su $USER_NAME -c 'xfconf_panel'
    exit_if_error " Oups :(" up
}


conf_xfce_nemo()
{
    print_message_arrow "Nemo comme gestionnaire de fichiers..."
    dpkg --verify nemo &> /dev/null
    exit_if_error " Impossible car Nemo n'est pas installé." silent_ok
    if [ ${default_file_manager} == "nemo" ]; then
        mkdir -p "/home/${USER_NAME}/.local/share/xfce4/helpers"
        chown -R "${USER_NAME}":"${USER_NAME}" "/home/${USER_NAME}/.local/share/xfce4/"
        cp ./system/custom-FileManager.desktop "/home/${USER_NAME}/.local/share/xfce4/helpers/."
        chmod -R 644 "/home/${USER_NAME}/.local/share/xfce4/helpers/custom-FileManager.desktop"
        if [ $? -eq 0 ]; then
            local filename="/home/${USER_NAME}/.config/xfce4/helpers.rc"
            if [ ! -f ${filename} ]; then
                echo "FileManager=custom-FileManager" >> "${filename}"
            else
                cat "${filename}" | grep "FileManager=" &> /dev/null
                if [ $? -eq 0 ]; then 
                    sed -i 's/.*FileManager=.*/FileManager=custom-FileManager/' "${filename}"
                else
                    echo "FileManager=custom-FileManager" >> "${filename}"
                fi
            fi
        fi
        export -f command_line_gsettings
        su $USER_NAME -c 'command_line_gsettings'
        exit_if_error " Erreur lors de l'appel de gsettings :(" up
    else
        echo "  Le gestionnaire de fichiers par défaut n'est pas"
        echo "  Nemo dans le fichier xfce_configuration."
        echo "  Rien n'a été modifié."
    fi
}


conf_firefox_dark()
{
    print_message_arrow "Adaptation de Firefox aux thèmes sombres..."
    if [ "${FIREFOX_DIRECTORY}" == "" ]; then
        FIREFOX_DIRECTORY="/home/${USER_NAME}/.mozilla/firefox"
    fi
    if [ ! -d ${FIREFOX_DIRECTORY} ]; then
        echo "    Création d'un profil pour Firefox..."
        export -f command_line_firefox_createprofile
        su $USER_NAME -c 'command_line_firefox_createprofile'
        exit_if_error " Impossible de créer un profil Firefox :(" up
    fi
    echo "    Recherche d'un profil Firefox..."
    PROFILE_DIRECTORY="${FIREFOX_DIRECTORY}/"$( ls "${FIREFOX_DIRECTORY}" | grep .default )
    exit_if_error " Impossible de trouver le profil Firefox :(" up
    echo "    Configuration du css pour Firefox..."
    mkdir -p "${PROFILE_DIRECTORY}/chrome/"
    cp ./system/userContent.css "${PROFILE_DIRECTORY}/chrome/."
    exit_if_error " Impossible de copier le fichier css :(" up
    chown -R "${USER_NAME}":"${USER_NAME}" "${PROFILE_DIRECTORY}/chrome/"
    chmod 700 "${PROFILE_DIRECTORY}/chrome/userContent.css"
}


command_line_firefox_createprofile()
{
    firefox -CreateProfile default
}
command_line_gsettings()
{
    # Command to select terminal used by Nemo (nemo contextual menu).
    gsettings set org.cinnamon.desktop.default-applications.terminal exec 'xfce4-terminal'
}
command_line_set-background() {
    dir=$(dirname "$0") && . "${dir}"/../xfce_configuration
    xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/workspace0/last-image -s "${BACKGROUNDS_TARGET_DIRECTORY}${DEFAULT_BACKGROUND_FILENAME}"
    xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/image-show -s true
    xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/last-image -s "${BACKGROUNDS_TARGET_DIRECTORY}${DEFAULT_BACKGROUND_FILENAME}"
    xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitor0/image-path -s "${BACKGROUNDS_TARGET_DIRECTORY}${DEFAULT_BACKGROUND_FILENAME}"
}
command_line_set-theme() {
    dir=$(dirname "$0") && . "${dir}"/../xfce_configuration
    xfconf-query -c xsettings -p /Net/IconThemeName -s ${IconThemeName}
    xfconf-query -c xsettings -p /Net/ThemeName -s ${ThemeName}
    xfconf-query -c xsettings -p /Xft/Antialias -s ${XftAntialias}
    xfconf-query -c xfwm4 -p /general/theme -s ${Xfwm_theme}
    xfconf-query -c xfwm4 -p /general/workspace_count -s ${Xfwm_workspace_count}
}
command_line_set-compositing() {
    xfconf-query -c xfwm4 -p /general/shadow_delta_width -s -18
    xfconf-query -c xfwm4 -p /general/shadow_delta_height -s 18
    xfconf-query -c xfwm4 -p /general/shadow_delta_x -s 14
    xfconf-query -c xfwm4 -p /general/shadow_delta_y -s -16
    xfconf-query -c xfwm4 -p /general/shadow_opacity -s 88
    xfconf-query -c xfwm4 -p /general/show_popup_shadow -s true
    xfconf-query -c xfwm4 -p /general/show_frame_shadow -s true
    xfconf-query -c xfwm4 -p /general/show_dock_shadow -s false
    xfconf-query -c xfwm4 -p /general/resize_opacity -s 88
    xfconf-query -c xfwm4 -p /general/move_opacity -s 90
    xfconf-query -c xfwm4 -p /general/popup_opacity -s 94
    xfconf-query -c xfwm4 -p /general/frame_opacity -s 90
    xfconf-query -c xfwm4 -p /general/use_compositing -s true
    xfconf-query -c xfwm4 -p /general/sync_to_vblank -s true
}
xfconf_panel() {
    
    # Removing    
    xfconf-query -c 'xfce4-panel' -p '/panels' --reset  --recursive  # delete all panels
    xfconf-query -c 'xfce4-panel' -p '/panels' --create --force-array --type int --set 1 # create a panel
    xfce4-panel --restart
    xfconf-query -c 'xfce4-panel' -p '/plugins' --reset --recursive # delete all plugins

    # Create new plugins from xfce_configuration file
    dir=$(dirname "$0") && . "${dir}"/../xfce_configuration
    if [ ${#panel1_plugin_list} -ge 3  ]; then
        n=1
        for name in ${panel1_plugin_list}; do
            xfconf-query -v -c xfce4-panel -p "/plugins/plugin-${n}" -n -t string -s "${name}"
            if [ ${name} == "separator" ]; then
                xfconf-query -c xfce4-panel -p "/plugins/plugin-${n}/expand" -n -t bool -s true  # expand=true
                xfconf-query -c xfce4-panel -p "/plugins/plugin-${n}/style" -n -t int -s 0  # 0=transparent, 1=separator, 2=handle, 3=dots
            fi
            if [ ${name} == "tasklist" ]; then
                xfconf-query -c xfce4-panel -p "/plugins/plugin-${n}/flat-buttons" -n -t bool -s true
                xfconf-query -c xfce4-panel -p "/plugins/plugin-${n}/show-handle" -n -t bool -s false
            fi
            n=$(( $n + 1 ))
        done
    fi
    
    # Add all plugins to panel
    if [ $n -gt 1 ]; then
        local long_list=$(for i in $(seq 1 $(( $n - 1 ))); do echo "-t int -s $i "; done | tr '\n' ' ')
        xfconf-query -c xfce4-panel -p /panels/panel-1/plugin-ids -n -a $(echo "${long_list}")
    fi
    PanelNumber=1
    xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/autohide -n -t bool -s false
    xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/background-alpha -n -t int -s 64
    xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/background-image -n -t string -s ""
    xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/background-style -n -t int -s 0   # 0 = default (theme) / 1 = custom background color / 2 = image
    xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/disable-struts -n -t bool -s false
    xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/enter-opacity -n -t int -s 100
    xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/leave-opacity -n -t int -s 100
    xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/length -n -t int -s 100
    xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/length-adjust -n -t bool -s true
    xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/mode -n -t int -s 0   # 0 = horizontal / 1 = vertical / 2 = deskbar
    xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/nrows -n -t int -s 1
    
    xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/position -n -t string -s "p=6;x=0;y=0"   # "p5;x=?;y=?" = use x & y coordinates / "p6;x=0;y=0" = screen top / "p7;x=0;y=0" = screen middle / "p8;x=0;y=0" = screen bottom
    xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/position-locked -n -t bool -s true
    xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/size -n -t int -s 30
    
    # Restart panels
    sleep 0.5
    xfce4-panel --restart
    sleep 1
}


#-----------------------------------------------------



#-------------------
# Debut du programme 
#-------------------


clear
print_title "PARTIE 2 - CONFIGURATION DE XFCE"


#echo "  Vérification des permissions..."
[ $EUID -eq 0 ]
exit_if_error "Le script doit être lancé par root." silent_ok
if [ "${XDG_CURRENT_DESKTOP}" != "XFCE" ]; then
    echo "  Avertissement : Xfce ne semble pas être actif."
fi

ask_select_username


while true; do
    pause
	clear
	print_title "PARTIE 2 - CONFIGURATION DE XFCE"
    print_menu_part2
    read -p "  Choix : " choix
    case $choix in
        1 ) conf_xfce_autologin;;
        2 ) conf_xfce_backgrounds ;;
        3 ) conf_xfce_theme ;;
        4 ) conf_xfce_panel ;;
        5 ) conf_xfce_nemo ;;
        6 ) conf_firefox_dark ;;
        0 ) break ;;
        #* ) echo "  Veuillez saisir une valeur du menu." ;;
    esac
done

echo " "
USER_NAME=""
exit 0


