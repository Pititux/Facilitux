#!/bin/bash


: '
    Copyright (C) 2017 Pierre Clavel (pititux@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'

VERSION="0.17"

FTUX_PATH="./../"
DEB_PACKAGES_DIRECTORY="deb_packages/"
BACKGROUNDS_SOURCE_DIRECTORY="backgrounds/"
SCRIPT_FILES_PATH="files/"
SYSTEM_FILES_PATH="files/system/"

SCRIPT_FILE_NAME_0="main.sh"
SCRIPT_FILE_NAME_1="part01_main.sh"
SCRIPT_FILE_NAME_2="part02_main.sh"
SCRIPT_FILE_NAME_3="part03_main.sh"
SOFTWARES_INSTALL_FILENAME="softwares_to_install"
SOFTWARES_REMOVE_FILENAME="softwares_to_remove"
SOFTWARES_KEYS_FILENAME="softwares_keys"
SOURCESLIST_FILENAME="sources.list"

SOURCESLIST_TEMP_FILENAME="sources.list_temp"

FIREFOX_DIRECTORY=""

PROXY=""
#PROXY="http://10.10.10.10:8080/"


